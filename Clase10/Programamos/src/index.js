var moment = require('moment'); // require

console.log('Horario local'+ moment().format());
console.log('Horario UTC' + moment.utc().format());
const diferencia = (moment().hour()-moment.utc().hour())-24;

console.log(`${moment.utc().format()} - ${moment().format()} es igual a ` + diferencia);
console.log('UTC offset: ' + (moment()));

const fechaInicial = moment('2000-01-01');

if (fechaInicial.isBefore('2050-01-01')){
    console.log('La fecha inicial es anterior al año 2050');
} else {
    console.log('La fecha inicial es después al año 2050');
}; 

