const express = require('express');
const router = express.Router();

const { agregarUsuario, obtenerUsuarios } = require('../models/usuario.model');

/**
 * @swagger
 * /usuarios:
 *  get:
 *      summary: Obtener todos los usuarios del sistema
 *      tags: [Usuarios]
 *      responses:
 *          200:
 *              description: Lista de usuarios del sistema
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          items:
 *                              $ref: '#/components/schemas/usuario'
 */
router.get('/', (req, res) => {    
/* const {username} = req.params
const usuarios = obtenerUsuarios()
const filtro = usuarios.find(u=> u.username ==username)
if (filtro.some(u => u.isAdmin = false)){
    res.send('usted no está autorizado')
} else */
res.json(obtenerUsuarios());
});

/**
 * @swagger
 * /usuarios:
 *  post:
 *      summary: Crea un usuario en el sistema
 *      tags: [Usuarios]
 *      requestBody:
 *          required: true
 *          content:
 *              application/json:
 *                  schema:
 *                      type: object
 *                      properties:
 *                         username:
 *                              type:string
 *                         password:
 *                              type:string
 *                         isAdmin:
 *                               type:string
 *                      $ref: '#/components/schemas/usuario'
 *      responses:
 *          201:
 *              description: Usuario creado
 *          401:
 *              description: usuario y contrasena incorrectos
 */
router.post('/', (req, res) => {
    agregarUsuario(req.body)
    res.json(obtenerUsuarios);
})


router.put('/:email', (req, res) => {
    console.log(req.body);
    res.json(usuario);
});

router.delete('/:email', (req, res) => {
    res.json('Usuario eliminado');
});



module.exports = router;