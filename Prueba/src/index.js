const express = require('express');
const app = express();
const basicAuth = require('express-basic-auth');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const usuarioRoutes = require('./routes/usuario.route');

const autenticacion = require('./middlewares/Autenticacion.middleware');
const swaggerOptions = require('./utils/swaggerOptions');


app.use(express.json());

const swaggerSpecs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpecs));

app.use(basicAuth({ authorizer: autenticacion }));

app.use('/usuarios', usuarioRoutes);

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });