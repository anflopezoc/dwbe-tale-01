//function funcionPrueba (nombre, apellido) {
//    console.log('Hola desde una función'+ nombre + ' ' + apellido)

//}

//funcionPrueba("Andrés","López")

//CONST y LET vs VAr
// declarado = 0;
// var texto1 = "Texto ejemplo"

// let texto2 ="Hola LET JS";
// texto2 = "Hola LET JS!!!"; 

// const texto3 = "Hola desde constate";

// if (true) {
//     let textoVarIf = 'Hola desde dentro del i'
//     console.log(textoVarIf)
//     console.log(texto2)
// }

// // var declarado = 1; //con let no puedo hacerlo

// function detntroDeFuncion () {
//     var texto4 = "Hola  texto 4";
//     console.log(texto4);
//     return texto4
// }

// console.log(detntroDeFuncion());

//Funciones flecha:

function saludar () {
    console.loge('Hola1')
}

let saludar2 = function saludar ( ) {
    console.log('Hola2')
}

let saludar3 = (e) => {
    console.log('Hola3')
}
//saludar();
//saludar2();

saludar3();

document.getElementById('btn').addEventListener('click', saludar3);

function suma(n1, n2) {
    console.log(n1+ n2);
}

let suma2 = function (n1, n2) {
    console.log(n1 + n2);
}

let suma3 = (n1, n2) => {
    console.log(n1 + n2)
}

let suma4 = (n1, n2) => console.log(n1 + n2);

let elDoble = n1 =>  n1 * 2; //cuando es un solo parámetro se puede quitar paréntesis y corteches.


suma(1, 2);
suma2(1, 2);
suma3(1, 2);
suma4(1, 2);

console.log(enDoble(5));