const {Router} =require('express');
const router = Router();

const {ObtenerAutores, NuevoAutor} = require('../models/Autores');
const autores = ObtenerAutores(); 

/**
 * @swagger
 * /autores:
 *  get:
 *      description: welcome to swagger
 *
 */
router.get('/autores', (req,res) =>{
    res.json(autores);
});

/**
 * @swagger
 * /autores:
 *  post:
 *      description: crea otro autor
 *
 */
router.post('/autores', (req, res) => {
    const { id, nombre, apellido, fechaDeNacimiento, libros} = req.body;
    if(id && nombre && apellido && fechaDeNacimiento && libros){
        const Autor = NuevoAutor(req.body);
        res.json(Autor);
    }else{
        res.json('No se agrego el nuevo autor');
    };
})

router.get('/autores/:id', (req,res) =>{
    const {id}= req.params;
    const filtro = autores.find(u => u.id == id)
    res.json(filtro);
});

router.put('/autores/:id', (req,res) => {
   const {id} = req.params;
   const autorAct = req.body
   autores.splice(id-1,1, autorAct);
   res.json(autores);
   console.log('Autor actualizado');
});

router.delete('/autores/:id', (req,res) => {
    const {id} = req.query;
    autores.splice(id, 1);
    res.json('Autor eliminado');
})

module.exports = router;