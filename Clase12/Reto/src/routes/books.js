const {Router} =require('express');
const router = Router();

const {ObtenerAutores, NuevoAutor} = require('../models/Autores');
const autores = ObtenerAutores(); 




router.get('/autores/:id/libros/:idLibro', (req,res) =>{
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
    const filtroLibro = filtro.libros.find( u => u.id == idLibro)
    res.json(filtroLibro); 
});

router.post('/autores/:id/libros/', (req,res)=> {
    const {id} =req.params;
    const filtro = autores.find(u => u.id == id);
    filtro.libros.push(req.body)
    res.json(filtro)
});


router.delete('/autores/:id/libros/:idLibro', (req,res) =>{
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
    filtro.libros.splice(idLibro-1,1)
    res.json(filtro); 
});

module.exports = router;