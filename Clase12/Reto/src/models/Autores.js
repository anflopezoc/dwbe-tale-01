const Autores = [
    {
        id: 1,
        nombre: "Jorge Luis",
        apellido: "Borges",
        fechaDeNacimiento: "24/08/1899",
        libros: [
            {
                id: 1,
                titulo: "Ficciones",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1944
            },
            {
                id: 2,
                titulo: "El Aleph",
                descripcion: "Se trata de uno de sus mas...",
                anioPublicacion: 1949
            }
        ]
    }
];

const ObtenerAutores = () => {
    return Autores;
};


const NuevoAutor = (Autor) => {
    Autores.push(Autor);
    return Autores;
};


module.exports = {ObtenerAutores, NuevoAutor}
