const express = require('express');
const app = express();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'reto',
        version: '1.0.0'
      }
    },
    apis: ['./middlewares/middlewares.js','./routes/books.js','./routes/authors.js'],
  };
  
  const swaggerDocs = swaggerJsDoc(swaggerOptions);

  app.use('/api-docs',
  swaggerUI.serve,
  swaggerUI.setup(swaggerDocs));

 
//middlewares
app.use(express.json());
app.use('/', require('./middlewares/middlewares'));

//Routes
app.use('/', require('./routes/books'))
app.use('/', require('./routes/authors'));

app.listen(5000, ()=> {console.log("Escuchando el puerto 5000")});


