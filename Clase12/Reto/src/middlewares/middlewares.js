
const {ObtenerAutores, NuevoAutor} = require('../models/Autores');
const autores = ObtenerAutores(); 

router.use('/autores/:id',(req,res,next) => {
    const {id} = req.params;

    if (autores.find(u => u.id != id)){
        res.send('ID de autor no encontrado')
    } else {
        next()
    };
        
});

router.use('/autores/:id/libros/:idLibro',(req,res,next) => {
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
    if (filtro.libros.find(u => u.id == idLibro)){
        next()
    } else 
        res.json('ID de libro invalido')     
        
});



router.use('/autores/:id/libros/:idLibro',(req,res,next) => {
    const {id,idLibro}= req.params;
    const filtro = autores.find(u => u.id == id);
   
});






module.exports = router;