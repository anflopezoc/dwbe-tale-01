const express = require('express');
const app = express();

app.use(express.json())

const usuarios = [
    {id:1, nombre: "Pepe", email: "pepe@nada.com"},
    {id:2, nombre: "Hugo", email: "hugo@nada.com"},
    {id:3, nombre: "Juan", email: "juan@nada.com"}
];

app.get('/usuarioporid/:id', (req, res) => {
    const id = req.params.id;
    const filtro = usuarios.find(u => u.id == id);
    if (filtro) {
        res.json(filtro)
    } else {
        res.status(404).json('Usuario no encontrado');
    }
});

app.use((req,res,next) => {
    console.log(req.url);
    next()
});
app.get('/usuariopornombre/:nombre', (req,res) => {
    const nombre = req.params.nombre;
    const filtro = usuarios.find(u => u.nombre = nombre);
    if(filtro){
        res.json(filtro)
    } else {
        res.status(404).json('Usuario no encontrado')
    }
})



app.listen(3000);