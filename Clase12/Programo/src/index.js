const express = require('express');
const app = express();
const { imprimirRequest, imprimirRequest2, imprimirRequest3 } = require('./middlewares/imprimir.middleware');

app.use(imprimirRequest);

app.get('/ejemplo2', imprimirRequest2, imprimirRequest3, (req, res) => {
    res.json('Respuesta ejemplo2 desde endpoint');
    console.log('Respuesta ejemplo2 desde endpoint');
});

app.get('/ejemplo', (req, res) => {
    res.json('Respuesta ejemplo desde endpoint');
    console.log('Respuesta ejemplo desde endpoint');
});

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });