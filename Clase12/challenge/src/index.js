const express = require('express')
const app = express();
const {losCursos} = require('./course/cursos');

app.listen(5000, () => console.log("listening on 5000"));

app.get('/estudiantes', (req, res) => {
    res.send([
      { id: 1, nombre: "Lucas", edad: 35 }
    ])
  }); 
  
  app.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });