lista_ordenada = [3, 4, 6, 7]
lista_reversada = [7, 6, 4, 3]
lista_original = [7, 6, 4, 3]


def bubble_sort(lista):
    print(f'lista de entrada {lista}')
    n = len(lista)
    cambios = True
    recorridos = 0
    while cambios:
        recorridos += 1
        cambios = False
        num_cambios = 0
        # for i in range(0, n - 1, 1):
        for i in range(recorridos - 1, n - 1, 1):
            actual = lista[i]
            siguiente = lista[i + 1]
            # if (actual > siguiente):
            if (actual < siguiente):
                lista[i] = siguiente
                lista[i + 1] = actual
                cambios = True
                num_cambios += 1
        print(f'num_cambios en recorrido {recorridos}: {num_cambios}')
        n -= 1
        # print(f'En el recorrido {recorridos} el for llegó hasta el índice {i}')
        print(f'lista después del recorrido {recorridos}: {lista}')
    return lista, recorridos


lista, recorridos = bubble_sort([5, 3, 6, 7, 4])
print('resultado', lista)
print('recorridos', recorridos)


    

