require('dotenv').config();
const express = require('express')
const app = express();

const PORT = process.env.PORT || 3000 

app.use(express.static('./src/public'));
app.use(express.json());

const listas_usuarios = ["user1","user2","user3"];

app.listen(PORT, () => {
    console.log('Inicio de servidor '+PORT);
});

app.get('/usuarios', function(req,res) {
    res.send(listas_usuarios)
});

app.post('/usuarios', function(req,res) {
    const { nombre } = req.body
    listas_usuarios.push(nombre)
    res.json('Usuario creado exitosamente')
});

app.put('/usuarios', function(req,res) {
   const{ index} = req.query;
   const {nombre } = req.body;
   listas_usuarios[index] = nombre;
   console.log(parametro, body);
   res.json('Usuario actualizado')
});

app.delete('/usuarios', function(req,res) {
    const { index} = req.query;
   listas_usuarios.splice(index,1);
   res.json('Usuario eliminado');
 });