const express = require('express');
const app = express();
const {obtenerMitadTelefonos,obtenerTelefonos,telefonoMenorPrecio,telefonoMayorPrecio,agruparTelefono} = require('./models/telefono');


app.get('/telefonos',(req,res) => {
    res.json(obtenerTelefonos())
});

app.get('/mitadtelefonos',(req,res) => {
    res.json(obtenerMitadTelefonos())
});

app.get('/menorpreciotelefonos',(req,res) => {
    res.json(telefonoMenorPrecio())
});

app.get('/mayorpreciotelefonos',(req,res) => {
    res.json(telefonoMayorPrecio())
});

app.get('/engrupos',(req,res) => {
    res.json(agruparTelefono())
});
app.listen(3000, ()=> {
    console.log('Escuchando en el puerto 3000');
})

