const telefonos = [
    {
        marca: "Samsung",
        gama: "Alta",
        modelo: "S11",
        OS:"Android",
        precio: 1200
    },
    {
        marca: "Iphone",
        modelo: "12 pro",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "iOs",
        precio: 1500
    },
    {
        marca: "Xiaomi",
        modelo: "Note 10s",
        gama: "Media",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 300
    },
    {
        marca: "LG",
        modelo: "LG el que sea",
        gama: "Alta",
        pantalla: "OLED",
        sistema_operativo: "Android",
        precio: 800
    }

];

const obtenerTelefonos = () =>{
    return telefonos;
}

const obtenerMitadTelefonos= () => {
   const mitad = Math.round(telefonos.length / 2);
   const mitadTelefonos = [];
   for (let index = 0; index < mitad; index++){
       const telefono =telefonos [index];
       mitadTelefonos.push(telefono);
   }
   return mitadTelefonos
};

const telefonoMenorPrecio = () => {
    let tel = 300;
    let price;
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        if(telefono.precio < tel){
            price = telefono;
            tel = telefono.precio;
        }
    }
    return price;

};

const telefonoMayorPrecio = () => {
    let tel = 0;
    let price;
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        if(telefono.precio > tel){
            price = telefono;
            tel = telefono.precio;
        }
    }
    return price;

};

const agruparTelefono = () => {
    let alta = [];
    let media = [];
    let baja = [];
    let todas = [];
    for (let index = 0; index < telefonos.length; index++) {
        const telefono = telefonos[index];
        if(telefono.gama == "Alta"){
            alta.push(telefono);
        }else if (telefono.gama == "Media"){
            media.push(telefono);
        }else if (telefono.gama == "baja"){
            baja.push(telefono);
        }else{console.log('gama no disponible')}
    }
    todas.push(alta);
    todas.push(media);
    todas.push(baja);
    return todas;

};


module.exports = {obtenerTelefonos,obtenerMitadTelefonos,telefonoMenorPrecio,telefonoMayorPrecio,agruparTelefono,};