const products = [
    {
        id:1,
        productName: "Cocacola",
        price: 3000,
        description: "bebiba gaseosa tamaño personal 250ml",
        q:1
    },
    {   
        id:2,
        productName: "Pony Malta",
        price: 2500,
        description: "bebiba gaseosa tamaño personal 250ml",
        q:1
    },
    {   
        id:3,
        productName: "Hamburguesa Full HD",
        price: 12500,
        description: "Hamburguesa de carne, doble queso, tocineta, guacamole, salsas de la casa",
        q:1
    },
    {   
        id:4,
        productName: "Papas",
        price: 12500,
        description: "Porcion de papas fritas por 400g",
        q:1
    }
];


const AllProducts = () => {
    return products;
}

const pushProduct = (productName,price,description) => {
    const valId = products[products.length-1].id+1;
    const newProduct =    {   
        id:valId,
        productName: productName,
        price: price,
        description: description,
        q:1
    }
   products.push(newProduct);
}

module.exports = {AllProducts, pushProduct}

