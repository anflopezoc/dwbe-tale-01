class persona {
    constructor (nombre, apellido, edad) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    fullname () {
        return this.nombre + " " + this.apellido;
    }

    es_mayor () {
        return this.edad >= 18;
    }
    
}

let persona1 = new persona ("Andres", "Lopez","30");

console.log(persona1.fullname());
console.log(persona1.es_mayor());

//una 